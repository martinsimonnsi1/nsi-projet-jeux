# -*- coding: utf-8 -*-
"""
Created on Sun Nov  5 16:06:32 2023
https://gitlab.com/martinsimonnsi1/nsi-projet-jeux/-/blob/main/projet_NSI_V1.py
@author: simon et martin
"""
liste_de_persos = []
with open("Characters.csv", mode='r', encoding='utf-8') as f:
    lines = f.readlines()
    key_line = lines[0].strip()
    keys = key_line.split(";")
    for line in lines[1:]:
        line = line.strip()
        values = line.split(';')
        dico = {}
        for i in range(len(keys)):
            dico[keys[i]] = values[i]
        liste_de_persos.append(dico)

from random import sample, choice

def jouer_partie_aleatoire():

    score = 0
    
    mines = sample([(i, j) for i in range(4) for j in range(4)], 3)


    

    def verifier_position(ligne, colonne):

        if (ligne, colonne) in mines:
            return True
        else:
            return False

    cases_sans_mines_restantes = 16 - len(mines)

    perdu = False

    while cases_sans_mines_restantes > 0 and not perdu:
        

        ligne = choice(range(4))
        colonne = choice(range(4))

        if verifier_position(ligne, colonne):
            
            perdu = True
        else:
            cases_sans_mines_restantes -= 1
            score += 1
    if perdu:
        score = score
    else:
        score = 50
       
    return score

jouer_partie_aleatoire()
for perso in liste_de_persos:
    score = 0
    for _ in range(10):
        score += jouer_partie_aleatoire()
    perso['score'] = score
liste_de_persos_triee = sorted(liste_de_persos, key=lambda x: x['score'])
for classement in enumerate(liste_de_persos_triee):
    print(f"{classement + 1} : {liste_de_persos[classement]['Name']}, {liste_de_persos[classement]['score']}")
